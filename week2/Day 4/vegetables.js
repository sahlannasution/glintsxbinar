let vegetables = ["Tomato", "Broccoli", "Kale", "Cabbage", "Apple"];

for (let i = 0; i < vegetables.length; i++) {
    if (vegetables[i] !== "Apple") {
        console.log(`${vegetables[i]} is a healthy food, it's definitely worth to eat.`);    
    }  
}
