const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}
// Function swap
function swap(data, left, right) {
  return [data[left], data[right]] = [data[right], data[left]]
}
// Should return array
function sortAscending(data) {
  var data = clean(data)
  let len = data.length
  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len - i - 1; j++) {
      if (data[j] > data[j + 1]) {
        //swap
        swap(data, j, j + 1)
      }
    }
  }
  return data;
}

// Should return array
function sortDecending(data) {
  // Code Here
  var data = clean(data)
  let len = data.length
  for (let i = 0; i < len; i++) {
    for (let j = 0; j < len - i - 1; j++) {
      if (data[j] < data[j + 1]) {
        //swap
        swap(data, j, j + 1)
      }
    }
  }
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
