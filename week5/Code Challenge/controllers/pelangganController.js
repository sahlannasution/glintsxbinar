const {
    pelanggan
} = require("../models")

class PelangganController {

    async getAll(req, res) {
        pelanggan.find({}).then(result => {
            res.json({
                status: "success",
                data: result
            })
        });
    }

    async getOne(req, res) {
        pelanggan.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        pelanggan.create({
            "nama": req.body.nama
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async update(req, res) {
        const query = {
            "nama": req.body.nama
        }
        pelanggan.findOneAndUpdate({
            _id: req.params.id
        }, query).then(() => {
            return pelanggan.findOne({
                _id: req.params.id
            })
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async delete(req, res) {
        pelanggan.findOneAndDelete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "success",
                data: null
            })
        })
    }

}

module.exports = new PelangganController