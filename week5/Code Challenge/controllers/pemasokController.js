const {
    pemasok
} = require("../models")

class PemasokController {

    async getAll(req, res) {
        pemasok.find({}).then(result => {
            res.json({
                status: "success",
                data: result
            })
        });
    }

    async getOne(req, res) {
        pemasok.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        pemasok.create({
            "nama": req.body.nama
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async update(req, res) {
        const query = {
            "nama": req.body.nama
        }
        pemasok.findOneAndUpdate({
            _id: req.params.id
        }, query).then(() => {
            return pemasok.findOne({
                _id: req.params.id
            })
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async delete(req, res) {
        pemasok.findOneAndDelete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "success",
                data: null
            })
        })
    }

}

module.exports = new PemasokController