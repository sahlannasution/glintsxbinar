const {
    barang,
    pemasok
} = require("../models")

class BarangController {

    async getAll(req, res) {
        barang.find({}).then(result => {
            res.json({
                status: "success",
                data: result
            })
        });
    }

    async getOne(req, res) {
        barang.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {

        const data = await Promise.all([
            pemasok.findOne({
                _id: req.body.id_pemasok
            })
        ])

        barang.create({
            "image": req.file === undefined ? "" : req.file.filename,
            "nama": req.body.nama,
            "harga": req.body.harga,
            "pemasok": data[0]
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async update(req, res) {

        const data = await Promise.all([
            pemasok.findOne({
                _id: req.body.id_pemasok
            })
        ])

        const query = {
            "image": req.file === undefined ? "" : req.file.filename,
            "nama": req.body.nama,
            "harga": req.body.harga,
            "pemasok": data[0]
        }
        barang.findOneAndUpdate({
            _id: req.params.id
        }, query).then(() => {
            return barang.findOne({
                _id: req.params.id
            })
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async delete(req, res) {
        barang.findOneAndDelete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "success",
                data: null
            })
        })
    }

}

module.exports = new BarangController;