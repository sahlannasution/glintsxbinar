const {
    barang,
    pemasok
} = require('../../models')

const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator')

const multer = require('multer')
const path = require('path')
const crypto = require('crypto')

const uploadDir = '/img/'
const storage = multer.diskStorage({
    destination: 'public' + uploadDir,
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) {
                return cb(err)
            }
            cb(null, raw.toString('hex') + path.extname(file.originalname))
        })
    }
})

const upload = multer({
    storage: storage,
    dest: uploadDir
})

module.exports = {
    create: [
        upload.single('image'), //upload image
        // cek apakah id_pemasok ada di collection pemasok
        check('id_pemasok').isLength({
            min: 1
        }).custom(value => {
            return pemasok.findById(value).then(b => {
                if (!b) {
                    throw new Error('ID pemasok tidak ada!');
                }
            })
        }),
        check('nama').isLength({
            min: 1
        }),
        check('harga').isLength({
            min: 3
        }).isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        upload.single('image'),

        check('id').isLength({
            min: 1
        }).custom(value => {
            return barang.findById(value).then(b => {
                if (!b) {
                    throw new Error('Id Barang tidak ada')
                }
            })
        }),
        check('id_pemasok').isLength({
            min: 1
        }).custom(value => {
            return pemasok.findById(value).then(pe => {
                if (!pe) {
                    throw new Error(`ID pemasok tidak ada`)
                }
            })
        }),
        check('harga').isLength({
            min: 3
        }).isNumeric(),
        check('nama').isLength({
            min: 1
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    delete: [
        check('id').isLength({
            min: 1
        }).custom(value => {
            return barang.findById(value).then(b => {
                if (!b) {
                    throw new Error("ID Barang tidak ada!")
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}