const {
    pelanggan
} = require('../../models')

const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator')


module.exports = {
    create: [
        check('nama').isLength({
            min: 1
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        check('id').isLength({
            min: 1
        }).custom(value => {
            return pelanggan.findById(value).then(p => {
                if (!p) {
                    throw new Error("ID Pelanggan tidak ada!")
                }
            })
        }),
        check('nama').isLength({
            min: 1
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    delete: [
        check('id').isLength({
            min: 1
        }).custom(value => {
            return pelanggan.findById(value).then(p => {
                if (!p) {
                    throw new Error("ID Pelanggan tidak ada!")
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req)
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}