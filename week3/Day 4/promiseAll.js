// Import fs module
const fs = require("fs");

/* Start make promise object */
const readFile = (options) => (file) =>
  new Promise((fulfill, reject) => {
    fs.readFile(file, options, (error, content) => {
      if (error) {
        return reject(error);
      }

      return fulfill(content);
    });
  });

const writeFile = (file, content) =>
  new Promise((resolve, reject) => {
    fs.writeFile(file, content, (err) => {
      if (err) return reject(err);
      return resolve();
    });
  });
/* Promise object end */

/* Make options variable for fs */
const read = readFile("utf-8");
/* End make options variable for fs */

/* Async function */
async function mergedContent() {
  try {
    /* This recommended */
    const result = await Promise.all([
      read("txt/content_1.txt"),
      read("txt/content_2.txt"),
      read("txt/content_3.txt"),
      read("txt/content_4.txt"),
      read("txt/content_5.txt"),
      read("txt/content_6.txt"),
      read("txt/content_7.txt"),
      read("txt/content_8.txt"),
      read("txt/content_9.txt"),
      read("txt/content_10.txt"),
    ]);
    /* End this
     recommended */
    // console.log(result);
    await writeFile("txt/result.txt", `promiseAll : ${result.join(" ")}`);
  } catch (error) {
    throw error;
  }

  // The best practice is:
  // return promise, not return value of promise
  // not also return use await
  return read("txt/result.txt");
}
/* Async function end */

// console.log(mergedContent()); // Promise (Pending)

// Start promise
mergedContent() // process read/write
  .then((result) => {
    console.log("Success to read and write file!\nContent: ", result); // If success read/write file
  })
  .catch((error) => {
    console.log("Failed to read/write file, content: ", error); // If error read/write file
  });
//   .finally(() => {
//     console.log("Mantap!"); // run after success or error
//   });