// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
    fs.readFile(file, options, (error, content) => {
        if (error) {
            return reject(error)
        }

        return fulfill(content)
    })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
        if (err) return reject(err)
        return resolve()
    })
})
/* Promise object end */

/* Make options variable for fs */
const read = readFile('utf-8')
/* End make options variable for fs */

/* Promise race */
Promise.race([read('txt/content_1.txt'), read('txt/content_2.txt'), read('txt/content_3.txt'), read('txt/content_4.txt'), read('txt/content_5.txt'), read('txt/content_6.txt'), read('txt/content_7.txt'), read('txt/content_8.txt'), read('txt/content_9.txt'), read('txt/content_10.txt')])
    .then((value) => {
        console.log(value);
        writeFile("txt/result.txt", `promiseRace : ${value}`);
    })
    .catch(error => {
        console.log(error);
    })
/* Promise race end */