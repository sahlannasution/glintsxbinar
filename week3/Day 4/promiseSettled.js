// Import fs module
const fs = require('fs');

/* Start make promise object */
const readFile = options => file => new Promise((fulfill, reject) => {
    fs.readFile(file, options, (error, content) => {
        if (error) {
            return reject(error)
        }

        return fulfill(content)
    })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
        if (err) return reject(err)
        return resolve()
    })
})
/* Promise object end */

/* Make options variable for fs */
const
    read = readFile('utf-8'),
    files = ['txt/content_1.txt', 'txt/content_2.txt', 'txt/content_3.txt', 'txt/content_4.txt', 'txt/content_5.txt', 'txt/content_6.txt', 'txt/content_7.txt', 'txt/content_8.txt', 'txt/content_9.txt', 'txt/content_10.txt']

// start promise allSettled
Promise.allSettled(files.map(file => read(`${file}`)))
    .then(results => {
        // const existsContent = results.filter(result => result.status)
        // isi dari existsContent: [{ status: 'fulfilled', value: 'isi content 1' }, ...]
        // dan seterusnya kecuali content 5
        // console.log(results);
        let a = ""
        for (let i = 0; i < results.length; i++) {
            if (a == "") {
                a += `${results[i].value}`
            } else {
                a += ` ${results[i].value}`
            }

        }
        console.log(a)
        writeFile("txt/result.txt", `promiseAllSettled : ${a}`);
    })