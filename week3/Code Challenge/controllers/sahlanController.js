// Make HelloController that will be used in helloRoutes
class SahlanController {

    // This function will be called in helloRoutes
    async sahlan(req, res) {
        try {
            console.log("You're accessing sahlan!")
            res.render('sahlan.ejs') // if success, will be rendering html file from views/sahlan.ejs
        } catch (e) {
            res.status(500).send(exception) // if error, will be display "500 Internal Server Error"
        }
    }

}

module.exports = new SahlanController; // We don't need to instance a object bacause exporting this file will be automatically to be an object