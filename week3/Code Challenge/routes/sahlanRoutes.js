// Import express
const express = require('express') // Express used to make API
const router = express.Router() // We need Router to make API in this folder
const SahlanController = require('../controllers/sahlanController.js') // Use SahlanController to call the function

router.get('/', SahlanController.sahlan) // If we run localhost:3000/sahlan, we will redirect to function sahlan in SahlanController class

module.exports = router; // Exporting router that can be used in app.js